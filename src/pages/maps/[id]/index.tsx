import CommonStyles from "../../../components/UI";
import CommonIcons from "../../../assets/icons";
import CommonImages from "../../../assets/images";
import { useHandleNavigate } from "../../../hooks/useHandleNavigate";
import Chatbot from "../../../components/formTemplate/detailMaps/Chatbot";
export default function DetailMap() {
  //! state
  const { handleBack } = useHandleNavigate();
  //! function
  const handleKeyPress = (event: any) => {
    console.log("a");
    if (event.key === " " || event.key === 32) {
      event.preventDefault(); // Prevent the default button behavior (e.g., form submission)
      // Perform your desired action here
      console.log("Spacebar pressed!");
    }
  };
  //! render

  return (
    <div className="h-full relative">
      <CommonStyles.ButtonUI
        className="bg-white text-black rounded-lg absolute z-1000 top-5 left-5"
        Icons={<CommonIcons.LeftOutlined />}
        size="large"
        onClick={handleBack}
      >
        Trở về
      </CommonStyles.ButtonUI>

      <CommonStyles.LeafletUI fullScreeen />

      <div
        className="absolute grid gap-3 z-1000 bottom-5 right-5"
        style={{ minWidth: 350 }}
      >
        <CommonStyles.CardUI
          title="Thông tin bay"
          extra={
            <CommonStyles.ButtonUI className="bg-black">
              Kết thúc
            </CommonStyles.ButtonUI>
          }
        >
          <div
            className="flex flex-col p-3 rounded-sm"
            style={{ background: "rgba(235, 237, 238, 1)" }}
          >
            <CommonStyles.TypographyUI className="text-lg">
              Bản đồ bay 1
            </CommonStyles.TypographyUI>
            <CommonStyles.TypographyUI>
              Mẫu sử dụng: Mẫu 1
            </CommonStyles.TypographyUI>
            <CommonStyles.TypographyUI>{`Chỉ số tốp: 023`}</CommonStyles.TypographyUI>
            <CommonStyles.TypographyUI>{`Ngày tạo: 04/04/2014`}</CommonStyles.TypographyUI>
            <CommonStyles.TypographyUI>{`Ghi chú:`}</CommonStyles.TypographyUI>
          </div>
        </CommonStyles.CardUI>

        <Chatbot />
      </div>
    </div>
  );
}
