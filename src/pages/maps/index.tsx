import React, { useState, useCallback } from "react";
import CommonStyles from "../../components/UI";
import CommonIcons from "../../assets/icons";
import { Space, Divider } from "antd";
import { dataPlane } from "../../mocks";
import CardUI from "../../components/UI/Card/CardUI";
import FormAdd from "../../components/maps/formAdd";
import { useNavigate } from "react-router-dom";

export default function Maps() {
  //! state
  const [add, setAdd] = useState(false);
  const navigate = useNavigate();
  //! function
  const addAction = () => {
    setAdd(!add);
  };
  const handleDetail = () => {
    return navigate("/maps/1");
  };
  //! render
  const renderSchedule = () => {
    return dataPlane.map((item, index) => {
      return (
        <div
          className="flex flex-col p-3 rounded-sm"
          style={{ background: "rgba(235, 237, 238, 1)" }}
        >
          <CommonStyles.TypographyUI className="text-lg">
            Bản đồ bay 1
          </CommonStyles.TypographyUI>

          <CommonStyles.TypographyUI>{`Duylb`}</CommonStyles.TypographyUI>
          <div className="flex justify-end">
            <CommonStyles.ButtonUI type="text" onClick={handleDetail}>
              Chi tiết
            </CommonStyles.ButtonUI>
          </div>
        </div>
      );
    });
  };
  const renderStateAdd = useCallback(() => {
    if (add) {
      return (
        <FormAdd
          handleClose={() => {
            setAdd(!add);
          }}
        />
      );
    }
    return (
      <div className="col-span-1 border-r pr-3">
        <div>
          <div className="flex justify-between">
            <CommonStyles.TypographyUI className="font-bold text-lg">
              Lịch trình bay
            </CommonStyles.TypographyUI>
            <CommonStyles.ButtonUI
              className="bg-black"
              Icons={<CommonIcons.PlusOutlined />}
              onClick={addAction}
            >
              Thêm mới
            </CommonStyles.ButtonUI>
          </div>
          <Divider />
        </div>

        <div
          className="flex flex-col gap-y-2 "
          style={{ height: 800, overflowY: "scroll" }}
        >
          {renderSchedule()}
        </div>
      </div>
    );
  }, [add]);
  return (
    <div className="grid grid-cols-3 gap-3 h-full">
      {renderStateAdd()}

      <div className="col-span-2">
        <div className="flex flex-col h-full gap-3">
          <div
            className={"w-full border rounded-lg " + (add ? "h-full" : "h-4/5")}
          >
            <CommonStyles.LeafletUI />
          </div>

          {!add && (
            <div className="flex flex-col p-3 border rounded-lg">
              <div className="flex flex-col">
                <CommonStyles.TypographyUI typographyType="title" level={5}>
                  Bản đồ bay 1
                </CommonStyles.TypographyUI>
                <CommonStyles.TypographyUI>{`Mẫu sử dung: Mẫu 1`}</CommonStyles.TypographyUI>
                <CommonStyles.TypographyUI>
                  {`Chỉ số tốp: 023`}
                </CommonStyles.TypographyUI>
                <CommonStyles.TypographyUI>
                  {`Ngày tạo: 04/04/2024`}
                </CommonStyles.TypographyUI>
                <CommonStyles.TypographyUI>
                  {`Ghi chú: `}
                </CommonStyles.TypographyUI>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
