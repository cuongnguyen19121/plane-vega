import React, { useState, useCallback } from "react";
import CommonStyles from "../../components/UI";
import CommonIcons from "../../assets/icons";
import { Space, Divider } from "antd";
import { dataPlane } from "../../mocks";
import CardUI from "../../components/UI/Card/CardUI";
import FormAdd from "../../components/maps/formAdd";
import FormAddTemplate from "../../components/formTemplate/formAdd";

export default function FormTemplate() {
  //! state
  const [add, setAdd] = useState(false);
  //! function
  const addAction = () => {
    setAdd(!add);
  };
  //! render
  const renderSchedule = () => {
    return dataPlane.map((item, index) => {
      return (
        <div
          className="flex flex-col p-3 rounded-sm"
          style={{ background: "rgba(235, 237, 238, 1)" }}
        >
          <CommonStyles.TypographyUI className="text-lg font-bold">
            Mẫu 1
          </CommonStyles.TypographyUI>

          <CommonStyles.TypographyUI>{`Duylb`}</CommonStyles.TypographyUI>
          <div className="flex justify-end">
            <CommonStyles.ButtonUI type="text">Chi tiết</CommonStyles.ButtonUI>
          </div>
        </div>
      );
    });
  };
  const renderStateAdd = useCallback(() => {
    if (add) {
      return (
        <FormAddTemplate
          handleClose={() => {
            setAdd(!add);
          }}
        />
      );
    }
    return (
      <div className="col-span-1 border-r pr-3">
        <div className="flex justify-between">
          <CommonStyles.TypographyUI className="font-bold text-lg">
            Mẫu mã hoá
          </CommonStyles.TypographyUI>
          <CommonStyles.ButtonUI
            className="bg-black"
            Icons={<CommonIcons.PlusOutlined />}
            onClick={addAction}
          >
            Thêm mới
          </CommonStyles.ButtonUI>
        </div>

        <Divider />

        <div className="flex flex-col gap-y-2">{renderSchedule()}</div>
      </div>
    );
  }, [add]);
  return (
    <div className="grid grid-cols-3 gap-3 h-full">
      {renderStateAdd()}

      <div className="col-span-2">
        <div className="flex flex-col h-full gap-3">
          <div className={"w-full border rounded-lg h-full"}>
            <CommonStyles.LeafletUI />
          </div>
        </div>
      </div>
    </div>
  );
}
