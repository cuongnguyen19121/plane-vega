import {
  MenuOutlined,
  PlusOutlined,
  LeftOutlined,
  AudioOutlined,
} from "@ant-design/icons";
const CommonIcons = {
  MenuOutlined: MenuOutlined,
  PlusOutlined: PlusOutlined,
  LeftOutlined: LeftOutlined,
  AudioOutlined: AudioOutlined,
};

export default CommonIcons;
