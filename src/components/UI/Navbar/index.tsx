import { useCallback, useEffect, useState } from "react";
import {
  HomeOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from "@ant-design/icons";
import { Button, Breadcrumb, Avatar, Badge, Typography, Tabs } from "antd";
import OutsideClickHandler from "react-outside-click-handler";
import useToggle from "../../../hooks/useToggle";
// import CommonIcons from "../../assets/icons";
// import { CommonStyles } from "../../components/ui";

const { Text } = Typography;
export default function Navbar(props: any) {
  //! state
  const [showUserMenus, setShowUserMenus] = useState<boolean>(false);
  const [showNotifications, setShowNotifications] = useState<boolean>(false);
  const { open, shouldRender, toggle } = useToggle();
  const options = [
    { id: 1, name: "Your Profile", action: () => {} },
    { id: 2, name: "Settings", action: () => {} },
    {
      id: 3,
      name: "Sign out",
      action: () => {
        toggle();
      },
    },
  ];
  //! function
  //! render
  //   const renderOptions = () => {
  //     return options?.map((item, index) => {
  //       return (
  //         <button
  //           key={index}
  //           className="block px-4 py-2 text-sm text-left w-full text-gray-700 hover:text-main-color"
  //           role="menuitem"
  //           id={`user-menu-item-${item.id}`}
  //           onClick={() => {
  //             item.action();
  //           }}
  //         >
  //           {t(`${item.name}`)}
  //         </button>
  //       );
  //     });
  //   };

  return (
    <nav className="flex justify-between bg-white">
      <div className="flex gap-x-2">
        <Button
          className="text-base !w-16 h-16 hover:!bg-transparent active:!bg-transparent"
          type="text"
          icon={
            props?.collapsed ? (
              <MenuUnfoldOutlined style={{ color: "#fff" }} />
            ) : (
              <MenuFoldOutlined style={{ color: "#fff" }} />
            )
          }
          onClick={() => props.setCollapsed(!props?.collapsed)}
        />
      </div>

      <div className="flex items-center px-4 gap-x-5">
        <div className="relative">
          <div
            className={
              "absolute right-0 z-10 mt-0 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" +
              (showUserMenus ? " transition duration-300 delay-300" : " hidden")
            }
            role="menu"
            aria-orientation="vertical"
            aria-labelledby="user-menu-button"
          >
            {/* {renderOptions()} */}
          </div>
        </div>
      </div>
      {/* {shouldRender && (
        <CommonStyles.ModalUI
          toggle={toggle}
          open={open}
          title={t("You want to exit the system?")}
          type="error"
          onConfirm={handleLogout}
        />
      )} */}
    </nav>
  );
}
