import {
  CircleMarker,
  MapContainer,
  Marker,
  Polyline,
  Popup,
  ScaleControl,
  TileLayer,
  ZoomControl,
} from "react-leaflet";
import { useEffect, useState } from "react";
import { Icon, LatLngBoundsExpression, LatLngExpression } from "leaflet";

import CommonImages from "../../../assets/images";
import { TDataMap } from "../../../interfaces";
import TypographyUI from "../Typography/TypographyUI";

interface IProps {
  data?: TDataMap[];
  zoom?: number;
  className?: string;
  fullScreeen?: boolean;
}
export default function LeafletUI(props: IProps) {
  //! state
  const { data, zoom, className, fullScreeen } = props;
  const position: LatLngExpression = [21.017069714364983, 105.85504793070729];
  const [documentHigh, setDocumentHigh] = useState<number>(
    document.documentElement.clientWidth ?? 0
  );
  const polyline: LatLngExpression[] = [
    [21.028511, 105.804817],
    [16.463713, 107.590866],
    [10.045162, 105.746857],
  ];

  const dataMock: { position: LatLngExpression }[] = [
    { position: [21.028511, 105.804817] },
    { position: [16.463713, 107.590866] },
    { position: [10.045162, 105.746857] },
  ];
  const limeOptions = { color: "rgba(76, 194, 255, 1)" };
  //! function
  const codingSpot = new Icon({
    iconUrl: CommonImages.position,
    iconSize: [25, 160],
    iconAnchor: [10, 100],

    // popupAnchor: [-25, -40],
  });

  //! render
  const renderMarker = () => {
    return dataMock?.map((item, index) => {
      return (
        <Marker position={item?.position} key={index} icon={codingSpot}>
          {/* <Popup>
            <div>
              <h3>{`Tên: ${item?.name}`}</h3>
              <TypographyUI>{`Địa chỉ: ${item?.address}`}</TypographyUI>
            </div>
          </Popup> */}
        </Marker>
      );
    });
  };

  return (
    <MapContainer
      center={position}
      zoom={6}
      maxZoom={13}
      minZoom={4}
      zoomControl={false}
      scrollWheelZoom={false}
      doubleClickZoom={true}
      style={{
        width: "100%",
        height: "100%",
        // minHeight: fullScreeen ? documentHigh : "",
        marginBottom: "12px",
      }}
    >
      <TileLayer url="https://mt1.google.com/vt/lyrs=s&hl=pl&&x={x}&y={y}&z={z}" />
      <ZoomControl position="bottomleft" />
      {renderMarker()}

      <Polyline pathOptions={limeOptions} positions={polyline} />
    </MapContainer>
  );
}
