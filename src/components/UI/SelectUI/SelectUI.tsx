import { Select, SelectProps, Space } from "antd";
import "./Select.css";
import { TOption } from "../../../interfaces";
interface IProps {
  disabled?: boolean;
  loading?: boolean;
  options: TOption[];
  hanleChange?: SelectProps["onChange"];
  className?: string;
  value?: string | number;
  variant?: "outlined" | "borderless" | "filled";
  handleSelect?: SelectProps["onSelect"];
}
export default function SelectUI(props: IProps) {
  //! state
  const {
    disabled,
    loading,
    options,
    hanleChange,
    className,
    value,
    handleSelect,
    variant,
    ...res
  } = props;
  //! function
  //! render
  return (
    <Select
      onChange={hanleChange}
      options={options}
      value={value}
      variant={variant}
      className={`m-w-10 ${className}`}
      onSelect={handleSelect}
      optionRender={(option) => <Space>{option.label}</Space>}
      {...res}
    />
  );
}
