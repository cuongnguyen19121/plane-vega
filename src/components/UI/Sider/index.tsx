import { Menu } from "antd";
import type { MenuProps } from "antd";

import { useNavigate } from "react-router-dom";
import CommonImages from "../../../assets/images";

const menu = [
  {
    label: "Bản đồ",
    key: "maps",
    icon: <img src={`${CommonImages.map}`} />,
    className: "!text-black !m-0 !rounded-none !w-full ",
  },
  {
    label: "Mẫu mã hoá",
    key: "form",
    icon: <img src={`${CommonImages.templa}`} />,
    className: "!text-black !m-0 !rounded-none !w-full ",
  },
];
export default function SiderMenu(props: any) {
  const navigate = useNavigate();

  const onClick: MenuProps["onClick"] = (e: any) => {
    navigate("/" + e?.key);
  };

  return (
    <Menu
      className="!bg-white !text-white !border-none"
      mode="inline"
      defaultSelectedKeys={["admin", "user"]}
      defaultOpenKeys={["admin", "user"]}
      onClick={(e) => onClick(e)}
      items={menu}
    />
  );
}
