import React from "react";
import CommonStyles from "../../UI";
import { Space, Divider, Form } from "antd";

interface IProps {
  handleClose: () => void;
}
export default function FormAdd(props: IProps) {
  //! state
  const { handleClose } = props;
  const [form] = Form.useForm();
  const initialValues = {
    title: "",
    valueTop: "",
    sortBy: "mau 1",
    note: "",
  };
  //! function
  //! render
  return (
    <div className="col-span-1 border-r pr-3">
      <div className="flex justify-between">
        <CommonStyles.TypographyUI className="font-bold text-lg">
          Thêm mới mẫu mã hoá
        </CommonStyles.TypographyUI>
      </div>

      <Divider />

      <div className="flex flex-col gap-y-2">
        <Form form={form} layout="vertical" initialValues={initialValues}>
          <CommonStyles.FormFieldUI
            name="title"
            label="Tiêu đề"
            component={
              <CommonStyles.InputUI
                className="bg-gray-100"
                maxLength={50}
                placeholder={"Nhập tiêu đề"}
              />
            }
          />

          <CommonStyles.FormFieldUI
            name="valueTop"
            label="Chỉ số tốp"
            component={
              <CommonStyles.InputUI
                className="bg-gray-100"
                maxLength={50}
                placeholder={"Nhập chỉ số tốp"}
              />
            }
          />

          <CommonStyles.FormFieldUI
            name="sortBy"
            label="Sort by"
            component={
              <CommonStyles.SelectUI
                variant="filled"
                className="bg-gray-100"
                options={[
                  {
                    label: "mau 1",
                    value: "mau1",
                  },
                  {
                    label: "mau 2",
                    value: "mau2",
                  },
                ]}
              />
            }
          />

          <CommonStyles.FormFieldUI
            name="note"
            label="Ghi chú"
            component={
              <CommonStyles.TextArea
                rows={7}
                maxLength={50}
                placeholder={"Nhập nội dung"}
                className="bg-gray-100"
              />
            }
          />

          <div className=" flex justify-end gap-3">
            <CommonStyles.ButtonUI
              className="bg-gray-100 text-black px-10"
              onClick={handleClose}
            >
              Hủy
            </CommonStyles.ButtonUI>

            <CommonStyles.ButtonUI className="bg-black text-white px-10">
              Lưu
            </CommonStyles.ButtonUI>
          </div>
        </Form>
      </div>
    </div>
  );
}
