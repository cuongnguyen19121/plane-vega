import React, { useRef, useState } from "react";
import CommonStyles from "../../UI";
import CommonImages from "../../../assets/images";
import CommonIcons from "../../../assets/icons";
import { dataChatBot } from "../../../mocks";

export default function Chatbot() {
  //! state
  const [dataBot, setDataBot] = useState(dataChatBot);
  const [valueText, setValueText] = useState("");
  const messagesEndRef = useRef<null | HTMLDivElement>(null);

  //! function
  const handleChangeText = (e: any) => {
    setValueText(e.target.value);
  };
  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({
      behavior: "smooth",
      block: "end",
    });
  };
  const onKeyUp = (e: any) => {
    if (e.keyCode === 13) {
      console.log("a");
      // handleSubmit(e);
      const data = [...dataBot];
      const item = {
        id: 1,
        text: valueText,
      };
      data.push(item);
      setDataBot(data);
      setValueText("");
      scrollToBottom();
    }
  };
  const handleMic = () => {
    console.log("aaaa");
  };
  //! render
  const suffix = (
    <CommonStyles.TooltipUI title="Voice">
      <CommonIcons.AudioOutlined
        onClick={handleMic}
        style={{
          fontSize: 16,
          color: "#000",
        }}
      />
    </CommonStyles.TooltipUI>
  );
  const UserChat = (item: any) => {
    return (
      <div className="flex items-center gap-3 row">
        <div
          className="border rounded-full text-center w-7 h-7"
          style={{ background: "rgba(235, 237, 238, 1)" }}
        >
          <img className="h-full" src={`${CommonImages.user}`} />
        </div>

        <div
          className="p-2 rounded-md"
          style={{ background: "rgba(235, 237, 238, 1)" }}
        >
          {item.item.text}
        </div>
      </div>
    );
  };
  const Bot = (item: any) => {
    return (
      <div className="flex items-center gap-3 flex-row-reverse" id="row">
        <div
          className="border rounded-full text-center w-7 h-7"
          style={{ background: "rgba(235, 237, 238, 1)" }}
        >
          <img className="h-full" src={`${CommonImages.bot}`} />
        </div>

        <div className="p-2 text-white bg-black text-end rounded-md w-72 break-words">
          {item.item.text}
        </div>
      </div>
    );
  };
  const renderChat = () => {
    return dataBot?.map((item, index) => {
      if (item.id === 2) {
        return <Bot item={item} key={index} />;
      }
      return (
        <div>
          <UserChat item={item} key={index} />
          <div ref={messagesEndRef} />
        </div>
      );
    });
  };
  return (
    <CommonStyles.CardUI title="Chatbot" height={450}>
      <div className="flex flex-col justify-between ">
        <div
          className="flex flex-col gap-4"
          style={{ height: 300, overflowY: "scroll" }}
        >
          {renderChat()}
        </div>
      </div>

      <div className="absolute w-11/12 bottom-5 text-center ">
        <CommonStyles.InputUI
          placeholder="Nhập nội dung..."
          variant="filled"
          suffix={suffix}
          onKeyUp={onKeyUp}
          value={valueText}
          onChange={handleChangeText}
        />
      </div>
    </CommonStyles.CardUI>
  );
}
