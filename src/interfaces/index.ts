import { LatLngExpression } from "leaflet";

export type TDataMap = {
  position: LatLngExpression;
  name?: string;
  address?: string;
};
export type TOption = {
  label: string;
  value: string | number;
};
