import { Routes, Route } from "react-router-dom";
import Maps from "../pages/maps";
import FormTemplate from "../pages/formTemplate";
import DefaultLayout from "../layouts/defaultLayout";
import DetailMap from "../pages/maps/[id]";

const Router = () => {
  return (
    <Routes>
      <Route element={<DefaultLayout />}>
        <Route path="/maps" element={<Maps />} />
        <Route path="/maps/:id" element={<DetailMap />} />

        <Route path="/form" element={<FormTemplate />} />
      </Route>

      {/* </Route> */}
    </Routes>
  );
};

export default Router;
