const plugin = require("tailwindcss/plugin");
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      zIndex: {
        1: "1",
        2: "2",
        3: "3",
        4: "4",
        5: "5",
        6: "6",
        7: "7",
        8: "8",
        9: "9",
        1000: "1000", // dropdown
        1020: "1020", // sticky
        1030: "1030", // fixed
        1040: "1040", // modal-backdrop
        1050: "1050", // offcanvas
        1060: "1060", // modal
        1070: "1070", // popover
        1080: "1080", // tooltip
        1500: "1500",
        2000: "2000",
      },
      colors: {
        "primary-100": "#005CA3",
        "primary-200": "#02E6183",
        "primary-300": "#3881AC",
        "primary-400": "#42A1D6",
        "primary-500": "#4CC2FF",

        "main-color": "#2F80ED",
        "secondary-color": "#E8ECF2",
        "complementary-color": "#E8ECF2",

        "s-line": "#DBE2ED",
      },
      boxShadow: {
        form: "0px 4px 4px 0px #E3E7ED;",
      },
      spacing: {
        4.5: "18px",
        5.5: "22px",
        6.5: "26px",
        7.5: "30px",
        8.5: "34px",
        9.5: "38px",
        10.5: "42px",
        11.5: "46px",
        12.5: "50px",
        13: "52px",
      },
    },
  },
  plugins: [
    plugin(function ({ addBase, addComponents, addUtilities, theme }) {
      addComponents({});
    }),
    plugin(({ addVariant, e }) => {
      addVariant("label-checked", ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          const eClassName = e(`label-checked${separator}${className}`); // escape class
          const yourSelector = 'input[type="radio"]'; // your input selector. Could be any
          return `${yourSelector}:checked ~ .${eClassName}`; // ~ - CSS selector for siblings
        });
      });
      addVariant("checkbox-checked", ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          const eClassName = e(`checkbox-checked${separator}${className}`); // escape class
          const yourSelector = 'input[type="checkbox"]';
          return `${yourSelector}:checked ~ .${eClassName}`; // ~ - CSS selector for siblings
        });
      });
    }),
  ],
};
